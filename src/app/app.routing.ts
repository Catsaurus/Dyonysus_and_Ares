
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AddItemComponent } from './dashboard/add-item/add-item.component';
import { LoginComponent } from './signup-login/login/login.component';
import { SignUpComponent } from './signup-login/signup/signup.component';
import { AuthGuard } from './shared/';
import { HomeComponent } from './home/home.component';
import { SignupLoginComponent } from './signup-login/signup-login.component';


const appRoutes: Routes = [
   { path: '', component: DashboardComponent, canActivate: [AuthGuard]},
   { path: 'home', component: HomeComponent},
   // { path: 'login', component: LoginComponent },
   { path: 'auth', component: SignupLoginComponent,
    children: [
        { path: 'login', component: LoginComponent },
        { path: 'signup', component: SignUpComponent }
    ]},

   // otherwise redirect to home
   { path: '**', redirectTo: 'home' }
];

export const routing = RouterModule.forRoot(appRoutes);
