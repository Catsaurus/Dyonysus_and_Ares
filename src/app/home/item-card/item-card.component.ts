import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { IItem } from './../../shared/models';
import { ItemService } from './../../shared/services';
import { formatTimestamp } from './../../shared/helpers';
import * as moment from 'moment';




@Component({
    selector: 'item-card',
    moduleId: module.id,
    templateUrl: 'item-card.component.html'
})

export class ItemCardComponent implements OnInit {

    @Input() item: IItem;
    @Output() removeItemFromItemsList = new EventEmitter<any>();

    bidVisible  = false;
    erroreid: boolean;
    interval = null;
    formattedTime: string;
    timeIsUp = false;

    constructor(private itemService: ItemService) { }

    ngOnInit() {
        if (!this.item.ended) {
            this.interval = setInterval(() => {
                this.formattedTime = this.formatTimestamp(this.item.timestamp);
            }, 1000);
        }
    }


    ngOnDestroy() {
        if (this.interval) {
            clearInterval(this.interval);
        }
    }
    toggleBidVisible(visible: boolean): void {
        this.bidVisible = visible;
    }

    formatTimestamp(timestamp: string): string {
        const now = moment();
        const diff = moment(timestamp).diff(now);
        if (diff <= 0) {
            this.itemService.update(this.item._id, { ended: true }, (res) => this.putItemCallback(res));
            clearInterval(this.interval);
            return '';
        }
        let seconds = Math.floor((diff / 1000));
        let minutes = Math.floor(seconds / 60);
        seconds = seconds % 60;
        let hours = Math.floor(minutes / 60);
        minutes = minutes % 60;
        const days = Math.floor(hours / 24);
        hours = hours % 24;
        let formattedTime = seconds + ' s ';
        if (minutes > 0) {
            formattedTime = minutes + ' m ' + formattedTime;
            if (minutes < 30 && !hours && !days && !this.timeIsUp) {
                this.timeIsUp = true;
            }
        }
        if (hours > 0) {
            formattedTime = hours + ' h ' + formattedTime;
        }
        if (days > 0 ) {
            formattedTime = days + ' p ' + formattedTime;
        }
        return formattedTime;
    }


    putItemCallback(response) {
        if (response === 'OK') {
            this.removeItemFromItemsList.emit({ _id: this.item._id });

        } else {
            this.erroreid = true;
            // kuvab erroreid
        }
    }

    formatTimestamp1(timestamp: string): string {
        return formatTimestamp(timestamp);
    }
}
