import { Injectable } from '@angular/core';

// tehniliselt pole vaja
@Injectable()
export class timeConverterHelper {
    constructor(
    ) {}
}


// date format {year: number, month: number, day: number }
// time format hh:mm
export const convertEndTimestamp = (date, time) => {
    // const hours: number = parseInt(time.split(':')[0]);
    // const minutes: number = parseInt(time.split(':')[1]);


    const dateObj: Date = new Date(date.year, date.month - 1, date.day, time.hour, time.minute);
    const dateString: string = dateObj.toISOString();
    return dateString;
};

// iso string timestamp -> DD/MM/YYYY - HH:mm
export const formatTimestamp = timestamp => {
    let timestring = '';
    const date = new Date(timestamp);
    timestring += date.getDate() >= 10 ? date.getDate() : '0' + date.getDate();
    timestring += '/';
    timestring += date.getMonth() >= 9 ? date.getMonth() + 1 : '0' + (date.getMonth() + 1);
    timestring += '/' + date.getFullYear() + ' - ';
    timestring += date.getHours() >= 10 ? date.getHours() : '0' + date.getHours();
    timestring += ':';
    timestring += date.getMinutes() >= 10 ? date.getMinutes() : '0' + date.getMinutes();
    return timestring;
};

export const hasEndArrived = (date) => {
    const current = new Date();
    console.log(date);
};
