export class Bid {
    _id: string;
    bidder: string;
    price: number;
    time: string;
}

export interface IBid {
    _id: string;
    bidder: string;
    price: number;
    time: string;
}
