import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { User, IItem, Item, ItemService } from '../shared/';



@Component({
    moduleId: module.id,
    templateUrl: 'dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})


export class DashboardComponent implements OnInit {

    currentUser: User;
    savedItems: Item[] = [];
    savedItems$: Observable<IItem[]>;
    publishedItems: Item[] = [];
    endedItems: Item[] = [];
    visible = false;
    items$: Observable<IItem[]>;
    items: IItem[];
    errored = false;
    errorMessage: String;

    constructor(private itemService: ItemService) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit() {
        this.getUserItems();
    }

    getUserItems() {
        this.items$ = this.itemService.getUserItems();
        this.items$.subscribe(
            items => {
                this.items = items;
                this.addToContainers(items);
            },
            error => this.errorMessage = <any>error);
    }

    addToContainers(items) {
        this.publishedItems = [];
        this.savedItems = [];
        this.endedItems = [];
        for (const item of items) {
            if (item.ended) {
                this.endedItems.push(item);
            } else if (item.published && !item.ended) {
                this.publishedItems.push(item);
            } else {
                this.savedItems.push(item);
            }
        }
    }

    toggleAddItemForm(visible: boolean): void {
        this.visible = visible;
    }

    sendSavedItemToPublicList($event, savedItem: Item) {
        this.savedItems = this.savedItems.filter(item => item._id !== $event.savedItem._id);
        this.publishedItems.push($event.savedItem);
    }

    sendPublishItemToEndedList($event) {
        this.publishedItems = this.publishedItems.filter(item => item._id !== $event.publishedItem._id);
        this.endedItems.push($event.publishedItem);
    }

    removeItemFromSavedList($event) {
        this.savedItems = this.savedItems.filter(item => item._id !== $event);
        this.itemService.delete($event).subscribe();
    }
}


