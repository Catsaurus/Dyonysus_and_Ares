import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AlertService, UserService } from '../../shared/';

@Component({
    selector: 'app-signup',
    moduleId: module.id,
    templateUrl: 'signup.component.html'
})

export class SignUpComponent {
    model: any = {};
    loading = false;

    constructor(
        private router: Router,
        private userService: UserService,
        private alertService: AlertService) { }

    signup() {
        this.loading = true;
        this.userService.create(this.model)
            .subscribe(
            data => {
                this.alertService.success('Registreeritud', true);
                this.router.navigate(['/auth/login']);

            },
            error => {
                this.alertService.error(error);
                this.loading = false;
            });
    }
}
