import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { ItemService } from './../../shared';

// import { IItem, Item } from './../../shared';
import { DashboardComponent } from './../dashboard.component';
// import { Observable } from 'rxjs/Observable';
// import { HttpClient } from '@angular/common/http';

import { IBid } from '../../shared/';
import { formatTimestamp } from '../../shared/';



@Component({
    selector: 'show-bids',
    moduleId: module.id,
    templateUrl: 'show-bids.component.html'
})

export class ShowBidsComponent implements OnInit {

    @Input() bids: IBid[];
    @Input() showBids: boolean;
    @Output() highestPrice = new EventEmitter<any>();

    constructor() { }

    ngOnInit() {
        this.sortByHighestBid(this.bids);
    }

    formatTimestamp(timestamp: string): string {
        return formatTimestamp(timestamp);
    }
    sortByHighestBid(bids) {
        if (bids !== undefined) {
            bids.sort((a, b) => {
                return b.price - a.price;
            });
        }
    }


}
