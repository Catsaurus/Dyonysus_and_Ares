import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { Headers, Response } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as _ from 'lodash';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { IItem } from '../models/item';
import { appConfig } from '../../app.config';
import { addJwtHeaders } from '../../shared/helpers';
import { concat } from 'rxjs/observable/concat';


@Injectable()
export class ItemService {
    url = appConfig.apiUrl + '/items';
    constructor(private http: HttpClient) { }
    /**
     * getAllPublicItems
     */
    getAll(): Observable<IItem[]> {
        return this.http.get(this.url + '/public', { headers: addJwtHeaders() })
            .map(data => _.values(data))
            .catch(this.handleErrorObservable);
    }

    getUserItems(): Observable<IItem[]> {
        return this.http.get(this.url + '/', { headers: addJwtHeaders() })
            .map(data => _.values(data))
            .catch(this.handleErrorObservable);
    }

    addBidToItem(model, _id: string, callback) {
        console.log(model);
        this.http.post(this.url + '/' + _id + '/bid',
            {
                'bidder': model.bidder,
                'price': model.price,
                'time': model.time,
            }, { headers: addJwtHeaders() })
            .subscribe(
            (val) => {
                callback('OK');
                console.log('POST call successful value returned in body',
                    val);
            },
            response => {
                callback('error');
                console.log('POST call in error', response);
            },
            () => {
                console.log('The POST observable is now completed.');
            });
    }


    create(model, callback) {
        this.http.post(this.url + '/create',
            {
                'title': model.title,
                'description': model.description,
                'user_id': model.user_id,
                'timestamp': model.timestamp,
                'published': model.published,
                'ended': false
            }, { headers: addJwtHeaders() })
            .subscribe(
            (val) => {
                callback('OK');

                console.log('POST call successful value returned in body',
                    val);
            },
            response => {
                callback('error');
                console.log('POST call in error', response);
            },
            () => {
                console.log('The POST observable is now completed.');
            });
    }

    delete(_id: string) {
        return this.http.delete(this.url + '/' + _id, { headers: addJwtHeaders() })
        .map(data => _.values(data))
        .catch(this.handleErrorObservable);
    }

    update(_id: string, body , callback) {
        this.http.put(this.url + '/' + _id, body, {headers: addJwtHeaders() })
        .subscribe(
            (val) => {
                callback('OK');

                console.log('PUT call successful value returned in body',
                    val);
            },
            response => {
                callback('error');
                console.log('PUT call in error', response);
            },
            () => {
                console.log('The PUT observable is now completed.');
            });

    }

    private extractData(res: Response) {
        const body = res.json();
        return body;
    }
    private handleErrorObservable(error: Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.message || error);
    }

}
