import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { IItem, IBid, Item } from './../../shared/models';
import { ItemService, formatTimestamp } from '../../shared/';




@Component({
    selector: 'ended-item-card',
    moduleId: module.id,
    templateUrl: 'ended-items.component.html'
})

export class EndedItemComponent implements OnInit {

    @Input() endedItems: IItem[];
     itemModel = new Item();
     item_id = this.itemModel._id;
     showBids = false;

    constructor(private itemService: ItemService) { }

    ngOnInit() {
        this.sortByHighestBid(this.endedItems);
    }

    formatTimestamp(timestamp: string): string {
        return formatTimestamp(timestamp);
    }

    removeItem(_id: string) {
        this.endedItems = this.endedItems.filter(item => item._id !== _id);
        this.itemService.delete(_id).subscribe();
    }

    onClickOffers(item: Item): void {
        this.showBids = !this.showBids;
        this.item_id = item._id === this.item_id ? null : item._id;
    }
    sortByHighestBid(endedItems) {
        if (endedItems.bids !== undefined) {
            endedItems.bids.sort((a, b) => {
                return b.price - a.price;
            });
        }
    }
}

