
## http://addboard.herokuapp.com



# Built with

* Angular 4
* Mongo DB (https://www.mongodb.com/), mlab.com for hosting MongoDB
* Node js(https://nodejs.org/en/), express js
* HTML5
* SCSS



## Build in localhost


* Download project 
    
    *  <b> https://gitlab.com/Catsaurus/Dyonysus_and_Ares.git</b> 

* Install packages 

    *  open commandline in root folder
    *  write <b>'npm install'</b> 

* Start database 

    * Download MongoDB from https://www.mongodb.com/download-center?jmp=nav#community (I used Community Server, Windows)
    * After installation, make a folder with name <b>'data'</b> to your <b> C:\ </b> disk 
    * Now make another folder inside that 'data' folder, called <b>'db'</b>. So when you go to that folder the path would be like: <b>'C:\data\db'</b>

    *  Search the mongoDB files in your computer
    *  Find the <b>'bin'</b> folder from mongoDB files
    *  path should look similar (still using windows): C:\Program Files\MongoDB\Server\3.4\bin
    *  Open commandline
    *  <b> Start mongod.exe file</b> 
    *  <b> Start mongo.exe file</b> 
    *  <i>Might need administrative rights</i>


* Start backend

    *  go to server folder in project (root folder->server)
    *  open commandline
    *  write <b> 'node server.js'</b> 

* Start frontend 

    *  open command line in root folder
    *  write <b> 'npm start'</b> 
    *  Go to <b>localhoast:4200</b>

* Application should be running now.
If any problems occourred, please contact me. 