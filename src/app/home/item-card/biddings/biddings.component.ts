import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Bid } from './../../../shared';
import { ItemService } from './../../../shared';
import { AlertService } from './../../../shared';


@Component({
    selector: 'add-bid',
    moduleId: module.id,
    templateUrl: './biddings.component.html'
})

export class BiddingsComponent implements OnInit {
    submitted = false;
    errorBidder = false;
    errorPrice = false;
    model = new Bid();
    @Input() item_id: string;
    @Input() visible: boolean;
    @Output() hide = new EventEmitter<any>();

    constructor(private itemService: ItemService, private alertService: AlertService) { }

    ngOnInit() {}

    onSubmit() {
        this.errorBidder = false;
        this.errorPrice = false;
        if (!this.model.price) {
            this.errorPrice = true;
        }
        if (!this.model.bidder) {
            this.errorBidder = true;
        }
        if (this.errorBidder === false && this.errorPrice === false) {
            this.submitted = true;
            this.model.time = new Date().toISOString();
            this.itemService.addBidToItem(this.model, this.item_id, (res) => this.postUserCallback(res));
        }
    }

    postUserCallback(response) {
        if (response === 'OK') {
            this.hide.emit();
            this.alertService.success('Pakkumine edastatud', true);
            this.model.bidder = null;
            this.model.price = null;
        } else {
            // this.errorid = true;
            // kuvab erroreid
        }
    }

}
