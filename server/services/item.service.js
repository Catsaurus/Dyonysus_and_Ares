
var config = require('./../config.json');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var Q = require('q');

var mongo = require('mongoskin');
var db = mongo.db(config.connectionString, { native_parser: true });
db.bind('items');

var service = {};

service.getAll = getAll;
service.getSavedPublishedItems = getSavedPublishedItems;
service.getUserItems = getUserItems;
service.getById = getById;
service.create = create;
service.update = update;
service.postBid = postBid;
service.delete = _delete;


module.exports = service;

/**
 * getAllPublished
 */
function getAll() {
    var deferred = Q.defer();
    db.items.find({published: true}).toArray(function (err, items) {
        if (err) deferred.reject(err.name + ': ' + err.message);
        items = _.map(items, function (items) {
            return _.omit(items);
        });
        deferred.resolve(items);
    });
    return deferred.promise;
}

function getSavedPublishedItems(){
    var deferred = Q.defer();
    db.items.find({ended: false }).toArray(function (err, items) {
        if (err) deferred.reject(err.name + ': ' + err.message);
        items = _.map(items, function (items) {
            return _.omit(items);
        });
        deferred.resolve(items);
    });
    return deferred.promise;
}


function getUserItems(_id) {
    var deferred = Q.defer();
    db.items.find({ user_id: _id }).toArray(function (err, items) {
        if (err) deferred.reject(err.name + ': ' + err.message);
        items = _.map(items, function (items) {
            return _.pickBy(items);
        });
        deferred.resolve(items);
    });
    return deferred.promise;
}



function getById(_id) {
    var deferred = Q.defer();
    db.items.findById(_id, function (err, item) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (item) {
            deferred.resolve(_.omit(item));
        } else {
            deferred.resolve();
        }
    });
    return deferred.promise;
}

function create(itemParam) {
    var deferred = Q.defer();
    db.items.insert(
        itemParam,
        function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc) {
                deferred.resolve(doc);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
};

function update(_id, itemParam) {
    var deferred = Q.defer();
    db.items.update(
        { _id: mongo.helper.toObjectID(_id) },
        { $set: itemParam },
        function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc) {
                deferred.resolve(doc);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
};

function postBid(_id, itemParam) {
    var deferred = Q.defer();
    db.items.update(
        { _id: mongo.helper.toObjectID(_id) },
        { $push: { "bids": itemParam } },
        function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);
            if (doc) {
                deferred.resolve(doc);
            } else {
                deferred.resolve();
            }
        });
    return deferred.promise;
};



function _delete(_id) {
    var deferred = Q.defer();
    db.items.remove(
        { _id: mongo.helper.toObjectID(_id) },
        function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });
    return deferred.promise;
}
