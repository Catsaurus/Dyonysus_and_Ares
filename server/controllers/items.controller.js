var config = require('./../config.json');
var express = require('express');
var router = express.Router();
var itemService = require('./../services/item.service');
 
// routes
router.get('/public', getAll);
router.get('/', getUserItems);
router.get('/:_id', getCurrent);
router.post('/create', create);
router.put('/:_id', update);
router.post('/:_id/bid', postBid);
router.delete('/:_id', _delete);

module.exports = router;

/*
* getPublicItems, /items/public
*/
function getAll(req, res) {
    itemService.getAll()
        .then(function (items) {
            res.send(items);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function getUserItems(req, res) {
    itemService.getUserItems(req.user.sub)
        .then(function (items) {
            console.log(items);
            res.send(items);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function getCurrent(req, res) {
    itemService.getById(req.params._id)
        .then(function (item) {
            if (item) {
                res.send(item);
            } else {
                res.sendStatus(404);
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function create(req, res) {
    itemService.create(req.body)
        .then(function (item) {
            res.status(200).send(item);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}
 

function update(req, res) {
    itemService.update(req.params._id, req.body)
        .then(function (item) {
            res.status(200).send(item);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function postBid(req, res) {
    itemService.postBid(req.params._id, req.body)
        .then(function (bid) {
            res.status(200).send(bid);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}
 
function _delete(req, res) {
    itemService.delete(req.params._id)
        .then(function () {
            // res.status(200).send({"message":"delete successful"});
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function checkItemEndDate() {
    itemService.getSavedPublishedItems()
        .then(function(items) {
            const currentDate = new Date();
            for (let item of items) {
                let itemDate = new Date(item.timestamp);
                if (itemDate.getTime() <= currentDate.getTime()) {
                    itemService.update(item._id, {ended: true})
                    .then(function () {
                    })
                }
            }
        })
}

setInterval(() => checkItemEndDate(), 2000);


