import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Item, } from './../../shared/models';
import { IItem } from './../../shared';
import { ItemService, formatTimestamp, convertEndTimestamp } from '../../shared/';

@Component({
    selector: 'saved-item-card',
    moduleId: module.id,
    templateUrl: 'saved-items.component.html'
})

export class SavedItemComponent implements OnInit {

    @Input() savedItem: IItem;
    @Output() sendSavedItemToPublicList = new EventEmitter<any>();
    @Output() removeItemFromSavedList = new EventEmitter<any>();

    errored = false;
    editSavedItem: IItem;
    enddate: Object;
    endtime: Object;

    constructor(private itemService: ItemService) { }

    ngOnInit() {
        const date = new Date(this.savedItem.timestamp);
        this.enddate = {year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDay()};
        this.endtime = {hour: date.getHours(), minute: date.getMinutes()};
        this.editSavedItem = null;
    }


    publishSavedItem(savedItem) {
        const body = { 'published': true };
        this.itemService.update(savedItem._id, body, (res) => this.putItemCallback(res, savedItem));
    }

    formatTimestamp(timestamp: string): string {
        return formatTimestamp(timestamp);
    }

    putItemCallback(response, savedItem: IItem) {
        if (response === 'OK') {
            this.sendSavedItemToPublicList.emit({ savedItem: savedItem });
        } else {
            this.errored = true;
            // kuvab erroreid
        }
    }

    editItem(savedItem: Item) {
        this.editSavedItem = savedItem;
    }

    endEditMode() {
        this.editSavedItem = null;
    }

    saveEditedItem(savedItem) {
        savedItem.timestamp = convertEndTimestamp(this.enddate, this.endtime);
        const body = {'timestamp' : savedItem.timestamp, 'title': savedItem.title,
                    'description': savedItem.description, 'published': false};

        this.itemService.update(savedItem._id, body, (res) => this.saveItemCallback(res, savedItem));
        this.editSavedItem = null;
    }


    saveItemCallback(response, savedItem: IItem) {
        if (response === 'OK') {
            // this.sendSavedItemToPublicList.emit({ savedItem: savedItem });
        } else {
            this.errored = true;
            // kuvab erroreid
        }
    }

    removeItem(_id: string) {
        this.removeItemFromSavedList.emit(_id);
    }


}


