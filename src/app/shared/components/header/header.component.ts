import { Component, OnInit, Input } from '@angular/core';
import { Router, CanActivate} from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { HeaderService } from './../../services';
import { User } from './../../models';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
})


export class HeaderComponent implements OnInit {
    currentUser: User;

    @Input() logged: boolean;
    subscription: Subscription;

    constructor(public router: Router, public headerService: HeaderService) {
        this.subscription = this.headerService.getLoggedIn().subscribe(loggedIn => { this.logged = loggedIn; }),
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }


    ngOnInit() {
        if (localStorage.getItem('currentUser')) {
            // logged in so return true
            this.logged = true;
        } else {
            this.logged = false;
        }
    }

    onLoggedout() {
        localStorage.removeItem('currentUser');
        this.logged = false;
    }

}
