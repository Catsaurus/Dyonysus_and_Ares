import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Item, IItem } from '../shared/';
import { ItemService } from '../shared/';


@Component({
    moduleId: module.id,
    templateUrl: 'home.component.html',
    styleUrls: ['./home.component.scss']
})


export class HomeComponent implements OnInit {

    items$: Observable<IItem[]>;
    items: IItem[];
    filteredItems: IItem[];
    errorMessage: String;
    activeItems: boolean;

    constructor(private itemService: ItemService) { }

    ngOnInit() {
        this.loadAllItems();
    }

    loadAllItems() {
        this.items$ = this.itemService.getAll();
        this.items$.subscribe(
            items => {
                this.items = items;
                this.showitems('activeItems');
            },
            error => this.errorMessage = <any>error);
    }

    removeItemFromItemsList($event) {
        this.items = this.items.map(item => item._id === $event._id ? { ...item, ended: true } : item);
        this.showitems('activeItems');
    }

    showitems(itemsToShow) {
        if (!this.items) {
            this.loadAllItems();
        }
        if (itemsToShow === 'activeItems') {
            this.filteredItems = this.items.filter(item => item.published === true && item.ended === false);
            this.activeItems = true;
        }
        if (itemsToShow === 'endedItems') {
            this.filteredItems = this.items.filter(item => item.ended === true && item.published === true);
            this.activeItems = false;
        }
    }


}
