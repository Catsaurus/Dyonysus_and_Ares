import { Injectable, NgModule } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class HeaderService {

    // constructor() { }
    private subject = new Subject<any>();

    setLoggedIn(loggedIn: boolean) {
        this.subject.next({ boolean: loggedIn });
    }

    getLoggedIn(): Observable<any> {
        return this.subject.asObservable();

    }

}
