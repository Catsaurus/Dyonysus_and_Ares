import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { IItem, Item } from './../../shared/models';
import { ItemService, formatTimestamp } from '../../shared/';




@Component({
    selector: 'published-item-card',
    moduleId: module.id,
    templateUrl: 'published-items.component.html'
})

export class PublishedItemComponent implements OnInit {

    @Input() publishedItems: IItem[];
    @Output() sendPublishItemToEndedList = new EventEmitter<any>();

    errored = false;
    items$: Observable<IItem[]>;
    items: IItem[];
    publishedItems$: Observable<IItem[]>;
    itemModel = new Item();
    item_id = this.itemModel._id;
    visible = false;
    showBids = false;
    constructor(private itemService: ItemService) { }

    ngOnInit() {
    }


    formatTimestamp(timestamp: string): string {
        return formatTimestamp(timestamp);
    }

    endEarlier(publishedItem: Item) {
        const body = { 'ended': true };
        this.itemService.update(publishedItem._id, body, (res) => this.endItemCallback(res, publishedItem));
    }

    onClickOffers(item: Item): void {
        this.showBids = !this.showBids;
        this.item_id = item._id === this.item_id ? null : item._id;
    }

    endItemCallback(response, publishedItem: IItem) {
        if (response === 'OK') {
            this.sendPublishItemToEndedList.emit({ publishedItem: publishedItem });
        } else {
            this.errored = true;
            // kuvab erroreid
        }
    }

}
