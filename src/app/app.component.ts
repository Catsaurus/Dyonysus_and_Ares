import { Component } from '@angular/core';
import { HeaderComponent } from './shared';

@Component({
   moduleId: module.id,
   selector: 'app-root',
   templateUrl: 'app.component.html',
   styleUrls: ['./app.component.scss']
})

export class AppComponent {
    title = 'app';
 }
