import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AlertService, AuthenticationService, UserService, ItemService, HeaderService} from './shared/';
import { AuthGuard, AlertComponent,  customHttpProvider } from './shared/';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './signup-login/login/login.component';
import { SignUpComponent } from './signup-login/signup/signup.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeaderComponent } from './shared';
import { AddItemComponent } from './dashboard/add-item/add-item.component';
import { BiddingsComponent } from './home/item-card/biddings/biddings.component';
import { SignupLoginComponent } from './signup-login/signup-login.component';
import { ShowBidsComponent } from './dashboard/show-bids/show-bids.components';
import { ItemCardComponent } from './home/item-card/item-card.component';
import { SavedItemComponent } from './dashboard/saved-items/saved-items.component';
import { PublishedItemComponent } from './dashboard/published-items/published-items.component';
import { EndedItemComponent } from './dashboard/ended-items/ended-items.component';




export function HttpLoaderFactory(http: HttpClient) {
}

@NgModule({
    imports: [
        NgbModule.forRoot(),
        BrowserModule,
        FormsModule,
        HttpModule,
        HttpClientModule,
        routing
    ],
    declarations: [
        AppComponent,
        AlertComponent,
        LoginComponent,
        SignUpComponent,
        DashboardComponent,
        HeaderComponent,
        HomeComponent,
        AddItemComponent,
        BiddingsComponent,
        SignupLoginComponent,
        ShowBidsComponent,
        ItemCardComponent,
        SavedItemComponent,
        PublishedItemComponent,
        EndedItemComponent
     ],
    providers: [
        customHttpProvider,
        AuthGuard,
        AlertService,
        AuthenticationService,
        UserService,
        ItemService,
        HeaderService
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }
