import { IBid, Bid } from './bid';

export class Item {
    _id: string;
    title: string;
    description: string;
    enddate: { year: number, month: number, day: number };
    endtime: string;
    timestamp: string;
    user_id: string;
    published: boolean;
    ended: boolean;
    bids: Bid[];
}

export interface IItem {
    _id: string;
    title: string;
    description: string;
    user_id: string;
    timestamp: string;
    published: boolean;
    ended: boolean;
    bids: IBid[];
}



