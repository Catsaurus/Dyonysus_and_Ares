import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Item } from './../../shared';
import { ItemService } from './../../shared';
import { UserService } from './../../shared';
import { AlertService } from './../../shared';
import { DashboardComponent } from '../dashboard.component';
import { convertEndTimestamp } from '../../shared/helpers';
import { NgbTimeStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'add-item',
    moduleId: module.id,
    templateUrl: './add-item.component.html',
    styleUrls: ['./add-item.component.scss']
})


export class AddItemComponent implements OnInit {

    // spinners = false;
    submitted = false;
    errored = false;
    model = new Item();
    currentUser = JSON.parse(localStorage.getItem('currentUser'));
    endtime = {hour: 23, minute: 59};

    @Input() visible: boolean;
    @Input() triggerLoadItems: Function;
    @Output() hide = new EventEmitter<any>();


    constructor(private itemService: ItemService,
                private alertService: AlertService,
                private dashboard: DashboardComponent) { }

    ngOnInit() {}

    onSubmit(param: boolean) {
        this.submitted = true;
        this.model.user_id = this.currentUser._id;
        this.model.published = param;
        this.model.timestamp = convertEndTimestamp(this.model.enddate, this.endtime);
        this.itemService.create(this.model, (res) => this.postUserCallback(res));
    }

    postUserCallback(response) {
        if (response === 'OK') {
            this.alertService.success('Lisatud', true);
            this.hide.emit();
            this.dashboard.getUserItems();
            this.model.title = null;  // teen väljad tühjaks
            this.model.description = null; // teen väljad tühjaks
            this.model.enddate = null; // teen väljad tühjaks
        } else {
            this.errored = true;
            // kuvab erroreid
        }
    }
}

