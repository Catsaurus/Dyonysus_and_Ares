require('rootpath')();
var express = require('express');
var app = express();
var path = require('path');
var cors = require('cors');
var bodyParser = require('body-parser');
var expressJwt = require('express-jwt');
var config = require('./config.json');


app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// production
// app.use(express.static(path.join(__dirname, '../dist'))); 


// use JWT auth to secure the api, the token can be passed in the authorization header or querystring
app.use(expressJwt({
    secret: config.secret,
    getToken: function (req) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            return req.headers.authorization.split(' ')[1];
        } else if (req.query && req.query.token) {
            return req.query.token;
        }
        return null;
    }
}) // for production 
// .unless({ path: ['/api/users/authenticate', '/api/users/register' , '/api/items/public', /^\/api\/items\/.+\/bid/] }));
    .unless({ path: ['/users/authenticate', '/users/register', '/items/public', /^\/items\/.+\/bid/, '/home'] }));

// for production
// app.use('/api/users', require('./controllers/users.controller'));
// app.use('/api/items', require('./controllers/items.controller'));

/* app.get('*', (req, res) => {
res.sendFile(path.join(__dirname, '../dist/index.html'));
});*/


// for development
app.use('/users', require('./controllers/users.controller'));
app.use('/items', require('./controllers/items.controller'));



// start server
var port = 4000;
var server = app.listen(process.env.PORT || port, function () {
    console.log('Server listening on port ' + port);
});